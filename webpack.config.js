const path = require("path");
const webpack = require("webpack");

const OUTPUT_DIR = "build";
const RESOURCES_DIR = "resources";
const PROD_ENV = "production";
const BASENAME_DEV = JSON.stringify("/");
const BASENAME_PRODUCTION = JSON.stringify("/site");

module.exports = (env) => {
    const production = env === PROD_ENV;
    return {
        target: "web",
        mode: "none",
        devtool: "source-map",
        plugins: [
            new webpack.DefinePlugin({
                BASENAME: production ? BASENAME_PRODUCTION : BASENAME_DEV,
            }),
        ],
        entry: [
            path.resolve(__dirname, "./html/index.html"),
            path.resolve(__dirname, "./src/index.tsx"),
            path.resolve(__dirname, "./css/index.less"),
        ],
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: "ts-loader",
                    exclude: /node_modules/,
                },
                {
                    test: /\.less$/,
                    use: [
                        {
                            loader: "style-loader",
                        },
                        {
                            loader: "css-loader",
                        },
                        {
                            loader: "less-loader",
                            options: {
                                strictMath: true,
                                noIeCompat: true,
                            },
                        },
                    ],
                },
                {
                    test: /\.css$/,
                    use: [
                        {
                            loader: "style-loader",
                        },
                        {
                            loader: "css-loader",
                        },
                    ],
                },
                {
                    test: /\.(ttf|eot|woff|woff2|otf)(\?v=[^\.]+\.[^\.]+\.[^\.]+)?$/,
                    use: {
                        loader: "file-loader",
                        options: {
                            name: "[name].[hash:8].[ext]",
                        },
                    },
                },
                {
                    test: /\.(jpe?g|svg|png)$/,
                    use: {
                        loader: "file-loader",
                        options: {
                            name: "[name].[hash].[ext]",
                        },
                    },
                },
                {
                    test: /\.html$/,
                    use: {
                        loader: "file-loader",
                        options: {
                            name: "[name].[ext]",
                        },
                    },
                },
            ],
        },
        resolve: {
            extensions: [".tsx", ".ts", ".js"],
        },
        output: {
            filename: "main.bundle.js",
            path: path.resolve(__dirname, OUTPUT_DIR),
            publicPath: "/",
        },
        devServer: {
            contentBase: [path.resolve(__dirname, OUTPUT_DIR), path.resolve(__dirname, RESOURCES_DIR)],
            watchContentBase: true,
            port: 5000,
            historyApiFallback: true,
        },
    };
};
