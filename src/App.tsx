import React from "react";
import * as Component from "./component";
import * as Logger from "./logger";
import * as Router from "react-router-dom";

interface AppState {
    theme: Component.LandingPageTheme;
}

class App extends React.Component<{}, AppState> {
    state: AppState = {
        theme: Component.LandingPageTheme.Clear,
    };

    toggleTheme() {
        const newTheme =
            this.state.theme === Component.LandingPageTheme.Clear
                ? Component.LandingPageTheme.Dark
                : Component.LandingPageTheme.Clear;
        this.setState({ theme: newTheme });
    }

    keyHandler(keyCode: Component.KeyCode) {
        switch (keyCode) {
            case Component.KeyCode.T:
                this.toggleTheme();
                break;
            default:
                Logger.log(Logger.LogLocation.App, `Key not handled: ${keyCode}`, Logger.LogType.Warning);
        }
    }

    render(): JSX.Element {
        return (
            <Router.HashRouter>
                <div className="App">
                    <Component.KeyController onKeyCode={(keyCode) => this.keyHandler(keyCode)}>
                        <Component.LandingPage theme={this.state.theme} />
                    </Component.KeyController>
                </div>
            </Router.HashRouter>
        );
    }
}

export default App;
