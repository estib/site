import * as React from "react";

export function breakLinedText(text: string): JSX.Element {
    return (
        <React.Fragment>
            {text.split("\n").map((segment, i) => (
                <React.Fragment key={i}>
                    {segment}
                    <br />
                </React.Fragment>
            ))}
        </React.Fragment>
    );
}
