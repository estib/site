interface XMLAttributes {
    [attrName: string]: any;
}

interface XMLDescription {
    name: string;
    attributes?: XMLAttributes;
}

export enum XMLChildType {
    Node = "xml-node",
    Text = "xml-text",
}

export interface XMLTextContent {
    type: XMLChildType.Text;
    content: string;
}

export type XMLChild = XMLTextContent | XML;

export interface XML {
    type: XMLChildType.Node;
    name: string;
    children: XMLChild[];
    attributes?: XMLAttributes;
    _id: string;
}

enum XMLSymbol {
    LeftBrace = "<",
    RightBrace = ">",
    Slash = "/",
    Equals = "=",
    Quote = '"',
}

enum XMLParseState {
    Initial,
    InTagDesc,
    InClosingTagDesc,
    InSelfClosingTagDesc,
}

class XMLParseError extends Error {
    constructor(msg: string) {
        super(`XML parse error. ${msg}`);
    }
}

export function findNodeByName(xml: XML, nodeName: string): XML | undefined {
    return xml.children.find((child) => child.type === XMLChildType.Node && child.name === nodeName) as XML | undefined;
}

function getCleanXML(rawXML: string): string {
    return rawXML.replace(/[\n]/g, "");
}

function cleanAttributeValue(value: string): string {
    return value.replace(/\"/g, "");
}

function isUnnecessaryContent(textContent: string): boolean {
    return textContent.trim() === "";
}

function createTagId(tagName: string, count: number): string {
    return [tagName, count].join("_");
}

/**
 * Won't work with spaces inside quotes.
 */
function parseAttributes(attributeList: string[]): XMLAttributes {
    const attributes: XMLAttributes = {};
    attributeList.forEach((attr) => {
        const indexOfEquals = attr.indexOf(XMLSymbol.Equals);
        if (indexOfEquals === -1) {
            throw new XMLParseError(`Attribute not parseable: ${attr}`);
        }
        const attrName = attr.substring(0, indexOfEquals);
        const attrValue = attr.substring(indexOfEquals + 1);
        const cleanAttrValue = cleanAttributeValue(attrValue);
        attributes[attrName] = cleanAttrValue;
    });
    return attributes;
}

function parseTagDescription(tagDescription: string): XMLDescription {
    const listedDescription = tagDescription.split(" ");
    const name = listedDescription[0];
    if (listedDescription.length === 1) {
        return {
            name,
        };
    }

    const attributes = parseAttributes(listedDescription.slice(1));
    return { name, attributes };
}

interface TagNameCount {
    [tagName: string]: number;
}

function getTagNameCount(tagNameCount: TagNameCount, tagName: string): number {
    return tagNameCount[tagName] || 0;
}

export class XMLParser {
    private static m_state: XMLParseState = XMLParseState.Initial;
    private static m_symbolBuffer: string[] = [];
    private static m_tagPath: string[] = [];
    private static m_tagIdPath: string[] = [];
    private static m_tagNameCount: TagNameCount = {};
    private static m_xml: XML | undefined = undefined;
    private static m_isInQuotes: boolean = false;

    private static resetParser() {
        this.m_state = XMLParseState.Initial;
        this.m_symbolBuffer = [];
        this.m_tagPath = [];
        this.m_tagIdPath = [];
        this.m_tagNameCount = {};
        this.m_xml = undefined;
        this.m_isInQuotes = false;
    }

    private static getBufferContent(trim: boolean): string {
        const content = this.m_symbolBuffer.join("");
        return trim ? content.trim() : content;
    }

    private static flushBuffer(trim: boolean): string {
        const bufferContent = this.getBufferContent(trim);
        this.m_symbolBuffer = [];
        return bufferContent;
    }

    private static getTagId(tagName: string): string {
        const tagCount = getTagNameCount(this.m_tagNameCount, tagName);
        const newCount = tagCount + 1;
        this.m_tagNameCount[tagName] = newCount;
        return createTagId(tagName, tagCount);
    }

    private static pushTag(selfClosing: boolean) {
        const tagDescription = this.flushBuffer(true);
        const { name, attributes } = parseTagDescription(tagDescription);
        const id = this.getTagId(name);
        this.composeXMLNode(name, id, attributes);
        if (!selfClosing) {
            this.m_tagPath.push(name);
            this.m_tagIdPath.push(id);
        }
    }

    private static popTag() {
        const tagName = this.flushBuffer(true);
        const closedTag = this.m_tagPath.pop();
        this.m_tagIdPath.pop();
        if (closedTag !== tagName) {
            throw new XMLParseError(`Expected tag: </${tagName}> Effective tag: </${closedTag}>`);
        }
    }

    private static pushSymbol(symbol: string, index: number) {
        if (symbol === XMLSymbol.LeftBrace) {
            throw new XMLParseError(`XML format error at ${index}`);
        }
        this.m_symbolBuffer.push(symbol);
    }

    private static getCurrentXMLNode(): XML | undefined {
        if (this.m_xml === undefined) {
            return undefined;
        }
        const tagRoot = this.m_tagPath[0];
        const tagRootId = this.m_tagIdPath[0];
        let node: XML = this.m_xml;
        if (node.name !== tagRoot || node._id !== tagRootId) {
            throw new XMLParseError(
                `Only one tag (<${node.name}> with id: ${node._id}) should wrap all other tags. Offending tag: <${tagRoot}> and id: ${tagRootId}`
            );
        }
        let i = 1;
        while (i < this.m_tagIdPath.length) {
            const childId = this.m_tagIdPath[i];
            let found = false;
            for (let j = 0; j < node.children.length; j++) {
                const nodeChild = node.children[j];
                if (nodeChild.type === XMLChildType.Text) {
                    continue;
                }
                if (nodeChild._id === childId) {
                    node = nodeChild;
                    found = true;
                    break;
                }
            }
            if (!found) {
                throw new XMLParseError(`Tag not found: ${childId}`);
            }
            i++;
        }

        return node;
    }

    // ############################################################################################################
    // XML COMPOSE

    private static composeXMLNode(tagName: string, tagId: string, attributes: XMLAttributes | undefined) {
        const newNode: XML = {
            type: XMLChildType.Node,
            name: tagName,
            children: [],
            attributes,
            _id: tagId,
        };
        if (this.m_xml === undefined) {
            this.m_xml = newNode;
        } else {
            const node = this.getCurrentXMLNode();
            if (node !== undefined) {
                node.children.push(newNode);
            }
        }
    }

    private static composeXMLText() {
        const textContent = this.flushBuffer(false);
        if (textContent !== "") {
            if (this.m_tagPath.length === 0 || this.m_xml === undefined) {
                throw new XMLParseError(`Text outside tag: "${textContent}"`);
            }
            const unnecessaryNode = isUnnecessaryContent(textContent);
            const node = this.getCurrentXMLNode();
            if (node !== undefined && !unnecessaryNode) {
                node.children.push({
                    type: XMLChildType.Text,
                    content: textContent,
                });
            }
        }
    }

    // ############################################################################################################
    // STATE HANDLERS

    private static handleInitialState(symbol: string, i: number) {
        switch (symbol) {
            case XMLSymbol.LeftBrace:
                this.composeXMLText();
                this.m_state = XMLParseState.InTagDesc;
                break;
            default:
                this.pushSymbol(symbol, i);
        }
    }

    private static handleInTagDesc(symbol: string, i: number) {
        switch (symbol) {
            case XMLSymbol.Quote:
                this.m_isInQuotes = !this.m_isInQuotes;
                this.pushSymbol(symbol, i);
                break;
            case XMLSymbol.Slash:
                if (this.m_isInQuotes) {
                    this.pushSymbol(symbol, i);
                    break;
                }
                const tagDescription = this.getBufferContent(true);
                if (tagDescription !== "") {
                    // self closing tag
                    this.m_state = XMLParseState.InSelfClosingTagDesc;
                    break;
                }
                this.m_state = XMLParseState.InClosingTagDesc;
                break;
            case XMLSymbol.RightBrace:
                if (this.m_isInQuotes) {
                    this.pushSymbol(symbol, i);
                    break;
                }
                this.m_state = XMLParseState.Initial;
                this.pushTag(false);
                break;
            default:
                this.pushSymbol(symbol, i);
        }
    }

    private static handleInClosingTagDesc(symbol: string, i: number) {
        switch (symbol) {
            case XMLSymbol.RightBrace:
                this.m_state = XMLParseState.Initial;
                this.popTag();
                break;
            default:
                this.pushSymbol(symbol, i);
        }
    }

    private static handleInSelfClosingTagDesc(symbol: string, i: number) {
        switch (symbol) {
            case XMLSymbol.RightBrace:
                this.m_state = XMLParseState.Initial;
                this.pushTag(true);
                break;
            default:
                throw new XMLParseError(`Self-closing tag formatting issue at index ${i}`);
        }
    }

    // ############################################################################################################
    // PARSE FUNCTION

    static parse(rawXML: string): XML | undefined {
        this.resetParser();
        const cleanXML = getCleanXML(rawXML);
        const length = cleanXML.length;

        for (let i = 0; i < length; i++) {
            const symbol = cleanXML.charAt(i);
            switch (this.m_state) {
                /**
                 * Initial parse state.
                 */
                case XMLParseState.Initial:
                    this.handleInitialState(symbol, i);
                    break;
                /**
                 * Inside a tag description.
                 */
                case XMLParseState.InTagDesc:
                    this.handleInTagDesc(symbol, i);
                    break;
                /**
                 * Inside closing tag description.
                 */
                case XMLParseState.InClosingTagDesc:
                    this.handleInClosingTagDesc(symbol, i);
                    break;
                /**
                 * Inside self-closing tag description.
                 */
                case XMLParseState.InSelfClosingTagDesc:
                    this.handleInSelfClosingTagDesc(symbol, i);
                    break;
            }
        }
        if (this.m_symbolBuffer.length > 0) {
            const remnantContent = this.flushBuffer(true);
            throw new XMLParseError(`Text outside tag: "${remnantContent}"`);
        }
        if (this.m_tagPath.length > 0) {
            throw new XMLParseError(`Missing closing tag for ${this.m_tagPath.join(", ")}`);
        }

        return this.m_xml;
    }
}
