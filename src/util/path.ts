import path from "path";

export function navigateWindow(navigatePath: string, local?: true) {
    const basename = path.join(window.location.origin, BASENAME);
    const url = local ? path.join(BASENAME, "#", navigatePath) : new URL(navigatePath, basename).href;
    window.location.href = url;
}

export function resolveUrlPath(navigatePath: string): string {
    return path.join(BASENAME, navigatePath);
}
