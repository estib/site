import React from "react";
import * as Logger from "../logger";

export enum KeyCode {
    T = 84,
}

interface KeyControllerProps {
    onKeyCode: (keyCode: KeyCode) => void;
}

export class KeyController extends React.Component<KeyControllerProps, {}> {
    componentDidMount() {
        document.addEventListener("keyup", (ev: KeyboardEvent) => this.keyUpHandler(ev));
    }
    componentWillUnmount() {
        document.removeEventListener("keyup", (ev: KeyboardEvent) => this.keyUpHandler(ev));
    }

    keyUpHandler(ev: KeyboardEvent) {
        switch (ev.keyCode) {
            case KeyCode.T:
                this.props.onKeyCode(KeyCode.T);
                break;
            default:
                Logger.log(
                    Logger.LogLocation.KeyController,
                    `Key code not handled: ${ev.keyCode}`,
                    Logger.LogType.Warning
                );
        }
    }

    render(): JSX.Element {
        return <React.Fragment>{this.props.children}</React.Fragment>;
    }
}
