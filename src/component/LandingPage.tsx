import React from "react";
import _ from "lodash";
import * as Router from "react-router-dom";

import * as Logger from "../logger";
import * as Mappings from "../mappings";
import * as Model from "../domain";
import * as Util from "../util";

import { ArticlePage, ArticleExplorer } from "./ArticlePage";
import { Curriculum } from "./Curriculum";
import { TextButton, TextButtonSize } from "./generic";

// ##################################################################################
// ROUTES

enum RoutePaths {
    Root = "/",
    CV = "/cv",
}

const Route: React.FC<Router.RouteProps> = (props) => {
    const routePath = typeof props.path === "string" ? Util.resolveUrlPath(props.path) : props.path;
    return (
        <Router.Route {...props} path={routePath}>
            {props.children}
        </Router.Route>
    );
};

// ##################################################################################
// SCROLL AREA

const LandingPageScrollArea: React.FC = (props) => {
    return (
        <div className="landing-page-scroll-area">
            <div className="landing-page-scroll-area-content">
                {props.children}
                <div className="landing-page-footer">Thanks.</div>
            </div>
        </div>
    );
};

// ##################################################################################
// MAIN COMPONENT - LANDING PAGE

const ARTICLE_KEY = "articleId";

interface ArticleMatchParams {
    [ARTICLE_KEY]: string | undefined;
}

function isArticleMatchParams(something: unknown): something is ArticleMatchParams {
    return (
        _.isObject(something) &&
        _.has(something, ARTICLE_KEY) &&
        (typeof (something as any)[ARTICLE_KEY] === "string" || (something as any)[ARTICLE_KEY] === undefined)
    );
}

function error(...msg: any[]) {
    Logger.log(Logger.LogLocation.LandingPage, msg.join(" "), Logger.LogType.Error);
}

export enum LandingPageTheme {
    Clear = "landing-page-clear-theme",
    Dark = "landing-page-dark-theme",
}

interface LandingPageProps {
    theme: LandingPageTheme;
}

interface LandingPageState {}

export class LandingPage extends React.Component<LandingPageProps, LandingPageState> {
    navigateTo(url: string) {
        Util.navigateWindow(url, true);
    }

    renderArticlePage(props: Router.RouteComponentProps): JSX.Element {
        const params = props.match.params as any;
        if (!isArticleMatchParams(params)) {
            return <React.Fragment />;
        }

        const articleId = params[ARTICLE_KEY];

        if (articleId === undefined) {
            // should not happen
            error("Article key not found in route params");
            return <React.Fragment />;
        }

        return (
            <LandingPageScrollArea>
                <ArticlePage articleId={articleId} />
            </LandingPageScrollArea>
        );
    }

    render(): JSX.Element {
        return (
            <div className={this.props.theme}>
                <div className="landing-page-header-bar">
                    <TextButton
                        size={TextButtonSize.Small}
                        label="home"
                        onClick={() => this.navigateTo(RoutePaths.Root)}
                    />
                </div>
                <Router.Switch>
                    {/* Render CV */}
                    <Route exact path={RoutePaths.CV}>
                        <LandingPageScrollArea>
                            <Curriculum />
                        </LandingPageScrollArea>
                    </Route>
                    {/* Render article */}
                    <Route
                        exact
                        path={`/:${ARTICLE_KEY}`}
                        component={(props: Router.RouteComponentProps) => this.renderArticlePage(props)}
                    />
                    {/* Render explorer */}
                    <Route exact path={RoutePaths.Root}>
                        <LandingPageScrollArea>
                            <ArticleExplorer
                                selectArticle={(articleId) => this.navigateTo(`/${articleId}`)}
                                manifest={Mappings.articleManifest}
                            />
                        </LandingPageScrollArea>
                    </Route>
                </Router.Switch>
            </div>
        );
    }
}
