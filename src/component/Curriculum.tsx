import React from "react";

import * as Article from "./ArticlePage";
import * as Mappings from "../mappings";
import * as Model from "../domain";
import * as Util from "../util";

import { Icon, IconSize } from "./generic";

// ##################################################################################
// CURRICULUM LINK
export interface CurriculumLinkItemProps extends Model.CurriculumLink {
    label?: string;
}

function getIconForLink(linkType: Model.CurriculumLinkType): Mappings.IconKey {
    switch (linkType) {
        case Model.CurriculumLinkType.GitLab:
            return "gitlab";
        case Model.CurriculumLinkType.LinkedIn:
            return "linkedin";
        case Model.CurriculumLinkType.Mail:
            return "mail";
    }
}

const CurriculumLinkItem: React.FC<CurriculumLinkItemProps> = (props) => {
    const icon = getIconForLink(props.type);

    return (
        <a className="curriculum-link-item" href={props.link}>
            <Icon icon={icon} original />
        </a>
    );
};

// ##################################################################################
// CURRICULUM LIST ITEM

interface CurriculumListItemProps extends Partial<Model.CurriculumItem> {
    icon?: Mappings.IconKey;
}

const CurriculumListItem: React.FC<CurriculumListItemProps> = (props) => {
    if (!!props.icon) {
        return (
            <li>
                <div className="curriculum-list-item-icon">
                    <Icon icon={props.icon} original size={IconSize.Medium} />
                </div>
            </li>
        );
    }
    return (
        <li>
            <div className="curriculum-list-item">
                {!!props.title && <div className="curriculum-list-item-title">{props.title}</div>}
                {!!props.subtitle && <div className="curriculum-list-item-subtitle">{props.subtitle}</div>}
                <div className="curriculum-list-item-description">{Util.breakLinedText(props.description ?? "")}</div>
            </div>
        </li>
    );
};

// ##################################################################################
// CURRICULUM LIST

interface CurriculumListProps {
    horizontal?: boolean;
}

const CurriculumList: React.FC<CurriculumListProps> = (props) => {
    if (!!props.horizontal) {
        return <div className="curriculum-list-container-horizontal">{props.children}</div>;
    }
    return (
        <div className="curriculum-list-container">
            <ul>{props.children}</ul>
        </div>
    );
};

// ##################################################################################
// CURRICULUM MAIN COMPONENT

export interface CurriculumProps {}

export class Curriculum extends React.Component<CurriculumProps, {}> {
    render(): JSX.Element {
        const curriculum = Mappings.curriculum;
        return (
            <div className="curriculum-container">
                <Article.Title size={Article.TitleSize.Big}>Jose E. Vega</Article.Title>
                <div className="curriculum-link-container">
                    {curriculum.links.map((l, i) => (
                        <CurriculumLinkItem type={l.type} link={l.link} key={i} />
                    ))}
                </div>
                <div className="curriculum-parragraph-container">
                    <Article.Paragraph size={Article.ParagraphSize.Medium}>{curriculum.introduction}</Article.Paragraph>
                </div>
                <CurriculumList horizontal>
                    {curriculum.skills.map((s, i) => (
                        <CurriculumListItem icon={s} key={i} />
                    ))}
                </CurriculumList>
                <Article.Title size={Article.TitleSize.Small}>Work</Article.Title>
                <CurriculumList>
                    {curriculum.work.map((w, i) => (
                        <CurriculumListItem {...w} key={i} />
                    ))}
                </CurriculumList>
                <Article.Title size={Article.TitleSize.Small}>Education</Article.Title>
                <CurriculumList>
                    {curriculum.education.map((e, i) => (
                        <CurriculumListItem title={e.title} description={e.description} key={i} />
                    ))}
                </CurriculumList>
            </div>
        );
    }
}
