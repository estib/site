import React from "react";

import * as Logger from "../logger";
import * as Model from "../domain";
import * as Mappings from "../mappings";
import * as Util from "../util";

function error(...msg: any[]) {
    Logger.log(Logger.LogLocation.Article, msg.join(" "), Logger.LogType.Error);
}

function warn(...msg: any[]) {
    Logger.log(Logger.LogLocation.Article, msg.join(" "), Logger.LogType.Warning);
}

// ##################################################################################
//

export enum TitleSize {
    Big = "landing-page-title-1",
    Medium = "landing-page-title-2",
    Small = "landing-page-title-3",
}

export interface TitleProps {
    size: TitleSize;
}

export const Title: React.FC<TitleProps> = (props) => {
    return <div className={props.size}>{props.children}</div>;
};

// ##################################################################################
//

export enum ParagraphSize {
    Big = "landing-page-paragraph-1",
    Medium = "landing-page-paragraph-2",
    Small = "landing-page-paragraph-3",
}

export interface ParagraphProps {
    size: ParagraphSize;
}

export const Paragraph: React.FC<ParagraphProps> = (props) => {
    return <div className={props.size}>{props.children}</div>;
};

// ##################################################################################
//

export interface QuoteProps {}

export const Quote: React.FC<QuoteProps> = (props) => {
    return <div className="landing-page-quote">{props.children}</div>;
};

// ##################################################################################
//

export interface AnchorProps {
    href: string;
}

export const Anchor: React.FC<AnchorProps> = (props) => {
    const onClick = () => {
        Util.navigateWindow(props.href);
    };
    return (
        <span className="landing-page-anchor" onClick={onClick}>
            {props.children}
        </span>
    );
};

// ##################################################################################
//

export enum ImageSize {
    Big = "landing-page-image-1",
    Medium = "landing-page-image-2",
    Small = "landing-page-image-3",
    /**
     * Article explorer item image
     */
    WideCover = "landing-page-image-wide-cover",
    TallCover = "landing-page-image-tall-cover",
}

export interface ImageProps {
    image: Mappings.ImageKey;
    size: ImageSize;
}

export const Image: React.FC<ImageProps> = (props) => {
    const backgroundImage = `url(${Mappings.images[props.image]})`;
    return <div className={props.size} style={{ backgroundImage }} />;
};

// ##################################################################################
// ARTICLE TRANSLATION

function translateArticle(articleContent: Model.ArticleComponentContent[]): JSX.Element[] {
    return articleContent.map((cm, i) => {
        if (typeof cm === "string") {
            return <React.Fragment key={i}>{cm}</React.Fragment>;
        }
        switch (cm.type) {
            case Model.ArticleComponentType.Image1:
                return <Image image={cm.image} size={ImageSize.Big} key={i} />;
            case Model.ArticleComponentType.Image2:
                return <Image image={cm.image} size={ImageSize.Medium} key={i} />;
            case Model.ArticleComponentType.Image3:
                return <Image image={cm.image} size={ImageSize.Small} key={i} />;
            case Model.ArticleComponentType.Title1:
                return (
                    <Title size={TitleSize.Big} key={i}>
                        {translateArticle(cm.content)}
                    </Title>
                );
            case Model.ArticleComponentType.Title2:
                return (
                    <Title size={TitleSize.Medium} key={i}>
                        {translateArticle(cm.content)}
                    </Title>
                );
            case Model.ArticleComponentType.Title3:
                return (
                    <Title size={TitleSize.Small} key={i}>
                        {translateArticle(cm.content)}
                    </Title>
                );
            case Model.ArticleComponentType.Paragraph1:
                return (
                    <Paragraph size={ParagraphSize.Big} key={i}>
                        {translateArticle(cm.content)}
                    </Paragraph>
                );
            case Model.ArticleComponentType.Paragraph2:
                return (
                    <Paragraph size={ParagraphSize.Medium} key={i}>
                        {translateArticle(cm.content)}
                    </Paragraph>
                );
            case Model.ArticleComponentType.Paragraph3:
                return (
                    <Paragraph size={ParagraphSize.Small} key={i}>
                        {translateArticle(cm.content)}
                    </Paragraph>
                );
            case Model.ArticleComponentType.Quote:
                return <Quote key={i}>{translateArticle(cm.content)}</Quote>;
            case Model.ArticleComponentType.Anchor:
                return (
                    <Anchor key={i} href={cm.href}>
                        {translateArticle(cm.content)}
                    </Anchor>
                );
        }
    });
}

// ##################################################################################
// ARTICLE PAGE

interface ArticlePageProps {
    articleId: string;
}

interface ArticlePageState {
    article: Model.Article | undefined;
}

export class ArticlePage extends React.Component<ArticlePageProps, ArticlePageState> {
    state: ArticlePageState = {
        article: undefined,
    };

    componentDidMount() {
        Model.fetchArticleXML(this.props.articleId)
            .then((article) => this.setState({ article }))
            .catch((e) => error(e));
    }

    renderDefault(): JSX.Element {
        warn("Rendering default article");
        return <React.Fragment />;
    }

    renderHead(head: Model.ArticleHead | undefined): JSX.Element {
        const title = head?.title;
        if (title === undefined) {
            return <React.Fragment />;
        }
        return (
            <div className="landing-page-main-title-container">
                <Title size={TitleSize.Big}>{title}</Title>
                <div className="landing-page-meta-information">
                    <p>{head?.author}</p>
                    <p>{head?.date}</p>
                </div>
            </div>
        );
    }

    renderBody(body: Model.ArticleComponentContent[] | undefined): JSX.Element {
        if (body === undefined) {
            return this.renderDefault();
        }
        return <React.Fragment>{translateArticle(body)}</React.Fragment>;
    }

    render(): JSX.Element {
        if (this.state.article === undefined) {
            return <h1>Nope, not found. Sorry about that</h1>;
        }

        const body = this.state.article?.body;
        const head = this.state.article?.head;
        return (
            <React.Fragment>
                {this.renderHead(head)}
                {this.renderBody(body)}
            </React.Fragment>
        );
    }
}

// ##################################################################################
// ARTICLE EXPLORER ITEM

enum ArticleExplorerItemSize {
    Big = "landing-page-explorer-item-1",
    Medium = "landing-page-explorer-item-2",
    Small = "landing-page-explorer-item-3",
}

interface ArticleExplorerItemProps {
    size: ArticleExplorerItemSize;
    title: string;
    paragraph: string;
    image: Mappings.ImageKey;
    onClick: () => void;
}

function getTitleSize(size: ArticleExplorerItemSize): TitleSize {
    switch (size) {
        case ArticleExplorerItemSize.Big:
            return TitleSize.Big;
        case ArticleExplorerItemSize.Medium:
            return TitleSize.Medium;
        case ArticleExplorerItemSize.Small:
            return TitleSize.Small;
    }
}

function getParagraphSize(size: ArticleExplorerItemSize): ParagraphSize {
    switch (size) {
        case ArticleExplorerItemSize.Big:
            return ParagraphSize.Big;
        case ArticleExplorerItemSize.Medium:
            return ParagraphSize.Medium;
        case ArticleExplorerItemSize.Small:
            return ParagraphSize.Small;
    }
}

function getImageSize(size: ArticleExplorerItemSize): ImageSize {
    switch (size) {
        case ArticleExplorerItemSize.Big:
            return ImageSize.WideCover;
        case ArticleExplorerItemSize.Medium:
            return ImageSize.Medium;
        case ArticleExplorerItemSize.Small:
            return ImageSize.TallCover;
    }
}

const ArticleExplorerItem: React.FC<ArticleExplorerItemProps> = (props) => {
    const titleSize = getTitleSize(props.size);
    const paragraphSize = getParagraphSize(props.size);
    const imageSize = getImageSize(props.size);
    return (
        <div className={props.size} onClick={props.onClick}>
            <div className="landing-page-explorer-item-image-container">
                <Image image={props.image} size={imageSize} />
            </div>
            <div className="landing-page-explorer-item-text-container">
                <Title size={titleSize}>{props.title}</Title>
                <Paragraph size={paragraphSize}>{props.paragraph}</Paragraph>
            </div>
        </div>
    );
};

// ##################################################################################
// ARTICLE EXPLORER

function getExplorerItemSize(size: Mappings.ArtExpManifestSize): ArticleExplorerItemSize {
    switch (size) {
        case Mappings.ArtExpManifestSize.Big:
            return ArticleExplorerItemSize.Big;
        case Mappings.ArtExpManifestSize.Medium:
            return ArticleExplorerItemSize.Medium;
        case Mappings.ArtExpManifestSize.Small:
            return ArticleExplorerItemSize.Small;
    }
}

interface ArticleExplorerProps {
    manifest: Mappings.ArtExpManifestItem[];
    selectArticle: (articleId: string) => void;
}

export class ArticleExplorer extends React.Component<ArticleExplorerProps, {}> {
    renderManifest(): JSX.Element {
        return (
            <React.Fragment>
                {this.props.manifest.map((item, index) => (
                    <ArticleExplorerItem
                        key={index}
                        title={item.title}
                        paragraph={item.paragraph}
                        size={getExplorerItemSize(item.size)}
                        image={item.image}
                        onClick={() => this.props.selectArticle(item.articleId)}
                    />
                ))}
            </React.Fragment>
        );
    }

    render(): JSX.Element {
        return <div className="landing-page-article-explorer">{this.renderManifest()}</div>;
    }
}
