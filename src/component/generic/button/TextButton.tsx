import React from "react";

export enum TextButtonSize {
    Big = "text-button-1",
    Medium = "text-button-2",
    Small = "text-button-3",
}

interface TextButtonProps {
    label: string;
    onClick: () => void;
    size?: TextButtonSize;
}

export const TextButton: React.FC<TextButtonProps> = (props) => {
    const cssClass = props.size ?? TextButtonSize.Medium;
    return (
        <div className={cssClass} onClick={props.onClick}>
            {props.label}
        </div>
    );
};
