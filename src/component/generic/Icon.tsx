import React from "react";

import * as Mappings from "../../mappings";

const BUTTON_ICON_COLOR = "#fff";

export enum IconSize {
    Big = "icon-big",
    Medium = "icon-medium",
    Small = "icon-small",
}

export interface IconProps {
    icon: Mappings.IconKey;
    color?: string;
    original?: boolean;
    size?: IconSize;
}

export class Icon extends React.Component<IconProps, {}> {
    render(): JSX.Element {
        const { icon, color, original } = this.props;
        const url = `url(${Mappings.icons[icon]})`;
        const iconClass: IconSize = this.props.size || IconSize.Small;

        if (!!original) {
            return (
                <div
                    className={`original-${iconClass}`}
                    style={{
                        backgroundImage: url,
                    }}
                ></div>
            );
        }

        return (
            <div
                className={iconClass}
                style={{
                    WebkitMask: `${url} no-repeat center`,
                    mask: `${url} no-repeat center`,
                    backgroundColor: !!color ? color : "",
                }}
            ></div>
        );
    }
}

export interface IconButtonProps extends IconProps {
    onClick: () => void;
}

export class IconButton extends React.Component<IconButtonProps, {}> {
    render(): JSX.Element {
        const color: string = this.props.color || BUTTON_ICON_COLOR;
        return (
            <div className="icon-button" onClick={() => this.props.onClick()}>
                <Icon icon={this.props.icon} color={color} />
            </div>
        );
    }
}
