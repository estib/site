import * as Mappings from "../mappings";

export enum CurriculumLinkType {
    Mail = "curriculum-mail",
    GitLab = "curriculum-gitlab",
    LinkedIn = "curriculum-linkedin",
}

export interface CurriculumLink {
    type: CurriculumLinkType;
    link: string;
}

export interface CurriculumItem {
    title: string;
    description: string;
    subtitle?: string;
    dateStart?: string;
    dateEnd?: string;
}

export interface CurriculumWorkItem extends CurriculumItem {}
export interface CurriculumEducationItem extends CurriculumItem {}
export interface CurriculumObject {
    links: CurriculumLink[];
    introduction: string;
    skills: Mappings.IconKey[];
    work: CurriculumWorkItem[];
    education: CurriculumEducationItem[];
}
