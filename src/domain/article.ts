import * as Util from "../util";
import * as Logger from "../logger";
import * as Mappings from "../mappings";

const ARTICLES_ENDPOINT = "articles";
const ARTICLE_TAG_NAME = "article";
const BODY_TAG_NAME = "body";
const HEAD_TAG_NAME = "head";

// ##################################################################################
// ARTICLE HEAD

export interface ArticleHead {
    title?: string;
    date?: string;
    author?: string;
}

enum ArticleHeadChild {
    Title = "title",
    Date = "date",
    Author = "author",
}

// ##################################################################################
// ARTICLE COMPONENT

export enum ArticleComponentType {
    Title1 = "title-1",
    Title2 = "title-2",
    Title3 = "title-3",
    Paragraph1 = "paragraph-1",
    Paragraph2 = "paragraph-2",
    Paragraph3 = "paragraph-3",
    Quote = "quote",
    Anchor = "anchor",
    Image1 = "image-1",
    Image2 = "image-2",
    Image3 = "image-3",
}

export type ArticleComponentContent = string | ArticleComponent;

export interface BaseArticleComponent {
    type: ArticleComponentType;
}

export interface BaseArticleComponentWithContent extends BaseArticleComponent {
    content: ArticleComponentContent[];
}

type ImageComponentArticleType =
    | ArticleComponentType.Image1
    | ArticleComponentType.Image2
    | ArticleComponentType.Image3;

export interface ImageComponent extends BaseArticleComponent {
    type: ImageComponentArticleType;
    image: Mappings.ImageKey;
}

export interface AnchorComponent extends BaseArticleComponentWithContent {
    type: ArticleComponentType.Anchor;
    href: string;
}

type TextComponentArticleType =
    | ArticleComponentType.Title1
    | ArticleComponentType.Title2
    | ArticleComponentType.Title3
    | ArticleComponentType.Paragraph1
    | ArticleComponentType.Paragraph2
    | ArticleComponentType.Paragraph3
    | ArticleComponentType.Quote;

export interface TextComponent extends BaseArticleComponentWithContent {
    type: TextComponentArticleType;
}

type ArticleComponent = TextComponent | AnchorComponent | ImageComponent;

export interface Article {
    head: ArticleHead;
    body: ArticleComponentContent[];
}

// ##################################################################################
// TAG NAME TO COMPONENT MAP

interface NameComponentMap {
    [nodeName: string]: ArticleComponentType;
}

const nodeToComponentType: NameComponentMap = {
    t1: ArticleComponentType.Title1,
    t2: ArticleComponentType.Title2,
    t3: ArticleComponentType.Title3,
    p1: ArticleComponentType.Paragraph1,
    p2: ArticleComponentType.Paragraph2,
    p3: ArticleComponentType.Paragraph3,
    q: ArticleComponentType.Quote,
    a: ArticleComponentType.Anchor,
    img1: ArticleComponentType.Image1,
    img2: ArticleComponentType.Image2,
    img3: ArticleComponentType.Image3,
};

function getArticleComponentType(childName: string): ArticleComponentType | undefined {
    const type = nodeToComponentType[childName];

    if (type === undefined) {
        Logger.log(Logger.LogLocation.Article, `Missing article component for ${childName}`, Logger.LogType.Warning);
        return undefined;
    }

    return type;
}

// ##################################################################################
// ARTICLE COMPONENT CREATORS

function createTextArticleComponent(type: TextComponentArticleType, child: Util.XML): ArticleComponentContent {
    return {
        type,
        content: getComponentContentArray(child),
    };
}

function createAnchorArticleComponent(child: Util.XML): ArticleComponentContent | undefined {
    if (child.attributes === undefined || child.attributes.href === undefined) {
        Logger.log(Logger.LogLocation.Article, "Missing 'href' attribute in anchor tag", Logger.LogType.Error);
        return undefined;
    }
    const href = child.attributes.href as string;
    return {
        type: ArticleComponentType.Anchor,
        content: getComponentContentArray(child),
        href,
    };
}

function createImageArticleComponent(
    type: ImageComponentArticleType,
    child: Util.XML
): ArticleComponentContent | undefined {
    if (child.attributes === undefined || child.attributes.key === undefined) {
        Logger.log(Logger.LogLocation.Article, "Missing 'key' attribute in image tag", Logger.LogType.Error);
        return undefined;
    }

    const imageKey = child.attributes.key as Mappings.ImageKey;
    return {
        type,
        image: imageKey,
    };
}

// ##################################################################################
// TRANSLATION

function getComponentContent(child: Util.XMLChild): ArticleComponentContent | undefined {
    switch (child.type) {
        case Util.XMLChildType.Text:
            return child.content;
        case Util.XMLChildType.Node:
            const type = getArticleComponentType(child.name);
            switch (type) {
                case undefined:
                    return undefined;
                case ArticleComponentType.Image1:
                case ArticleComponentType.Image2:
                case ArticleComponentType.Image3:
                    return createImageArticleComponent(type, child);
                case ArticleComponentType.Anchor:
                    return createAnchorArticleComponent(child);
                case ArticleComponentType.Title1:
                case ArticleComponentType.Title2:
                case ArticleComponentType.Title3:
                case ArticleComponentType.Paragraph1:
                case ArticleComponentType.Paragraph2:
                case ArticleComponentType.Paragraph3:
                case ArticleComponentType.Quote:
                    return createTextArticleComponent(type, child);
            }
    }
}

function getComponentContentArray(node: Util.XMLChild | undefined): ArticleComponentContent[] {
    if (node === undefined) {
        return [];
    }
    if (node.type === Util.XMLChildType.Node) {
        const children = node.children;
        const articleComponents = children.map((child) => getComponentContent(child)).filter((c) => c !== undefined);
        return articleComponents as ArticleComponentContent[];
    }
    return [node.content];
}

// ##################################################################################
// PARSING

function getHeadChildConten(children: Util.XMLChild[]): string {
    const content: string[] = [];
    children.forEach((c) => {
        switch (c.type) {
            case Util.XMLChildType.Node:
                Logger.log(Logger.LogLocation.Article, `Node inside head child: ${c.name}`, Logger.LogType.Warning);
                break;
            case Util.XMLChildType.Text:
                content.push(c.content);
        }
    });
    return content.join("");
}

function parseHeadChild(child: Util.XMLChild): ArticleHead {
    if (child.type !== Util.XMLChildType.Text) {
        switch (child.name) {
            case ArticleHeadChild.Author:
                return { author: getHeadChildConten(child.children) };
            case ArticleHeadChild.Date:
                return { date: getHeadChildConten(child.children) };
            case ArticleHeadChild.Title:
                return { title: getHeadChildConten(child.children) };
        }
    }
    return {};
}

function parseHead(node: Util.XMLChild | undefined): ArticleHead {
    const head: ArticleHead = {};

    if (
        node === undefined ||
        node.type === Util.XMLChildType.Text || // bad xml format
        node.name !== HEAD_TAG_NAME // should not happen
    ) {
        return head;
    }

    for (let i = 0; i < node.children.length; i++) {
        const child = node.children[i];
        const parsedChild = parseHeadChild(child);
        Object.assign(head, parsedChild);
    }

    return head;
}

function parseArticle(rawXML: string): Article | undefined {
    const xml = Util.XMLParser.parse(rawXML);

    if (xml !== undefined && xml.name === ARTICLE_TAG_NAME) {
        const xmlBody = Util.findNodeByName(xml, BODY_TAG_NAME);
        const xmlHead = Util.findNodeByName(xml, HEAD_TAG_NAME);
        return {
            head: parseHead(xmlHead),
            body: getComponentContentArray(xmlBody),
        };
    }

    return undefined;
}

// ##################################################################################
// FETCH

export function fetchArticleXML(articleID: string): Promise<Article> {
    return fetch(`${ARTICLES_ENDPOINT}/${articleID}.xml`)
        .then((response) => response.text())
        .then((content) => parseArticle(content))
        .then((article) => article || Promise.reject("Couldn't parse article"))
        .catch((e) => Promise.reject(e));
}
