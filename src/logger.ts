export enum LogLocation {
    App = "[APP]",
    KeyController = "[KEY CONTROLLER]",
    Article = "[ARTICLE]",
    LandingPage = "[LANDING PAGE]",
}

export enum LogType {
    Standard,
    Warning,
    Error,
}

const DEV_ORIGIN = "http://localhost:5000";

export function log(location: LogLocation, msg: string, type?: LogType) {
    const logType = type || LogType.Standard;
    const isDev = window.origin === DEV_ORIGIN;
    switch (logType) {
        case LogType.Standard:
            if (isDev) {
                // eslint-disable-next-line no-console
                console.log(`${location} - ${msg}`);
            }
            break;
        case LogType.Warning:
            if (isDev) {
                // eslint-disable-next-line no-console
                console.warn(`${location} - ${msg}`);
            }
            break;
        case LogType.Error:
            // eslint-disable-next-line no-console
            console.error(`${location} - ${msg}`);
            break;
    }
}
