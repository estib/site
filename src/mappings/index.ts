import img from "./images.json";
import ic from "./icon.json";
import artMan from "./articleManifest.json";
import c from "./curriculum.json";

import { CurriculumObject } from "../domain";

export const images = img;
export type ImageKey = keyof typeof images;

export const icons = ic;
export type IconKey = keyof typeof icons;

// Article explorer manifest

export enum ArtExpManifestSize {
    Big = "big",
    Medium = "medium",
    Small = "small",
}

export interface ArtExpManifestItem {
    title: string;
    paragraph: string;
    size: ArtExpManifestSize;
    articleId: string;
    image: ImageKey;
}

export const articleManifest = artMan as ArtExpManifestItem[];
export const curriculum = c as CurriculumObject;
